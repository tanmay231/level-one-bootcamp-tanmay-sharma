//WAP to find the sum of n fractions.
#include <stdio.h>
int gcd(int a,int b);
struct frac{
    int num;
    int den;
};
int lcm(struct frac a[],int n);
void printt(int s,int l,struct frac a[],int n);
void input_arr(int n,struct frac a[]);
int gcd(int a, int b);
int sum(struct frac a[],int n);
int main()
{
    
    int n;
    printf("enter no. of numbers to be added:");
    scanf("%d",&n);
     int s,l;
    struct frac a[n];
    input_arr(n,a);
    s=sum(a,n);
    l=lcm(a,n);
    printt(s,l,a,n);
    
    return 0;
}
int lcm(struct frac a[],int n){
 int gc,lcc=a[0].den;
  for (int i = 0; i < n; i++) 
        lcc = ((a[i].den * lcc) / (gcd(a[i].den, lcc))); 
  return lcc;
   }

void input_arr(int n,struct frac a[]){
    for(int i =0;i<n;i++){
       printf("enter numerator %d:",i+1);
       scanf("%d",&a[i].num);
       printf("enter denominator %d:",i+1);
      scanf("%d",&a[i].den);
     }
  }

 void printt(int s,int l,struct frac a[],int n)
   {
    int j=0;
printf("sum of");
for(int i=0;i<n;i++){
      for( j;j<=i;j++)
            printf("%d/%d",a[j].num,a[j].den);
       if((i+1)!=n)
         printf("+");
       }

     printf("  is %d/%d",s,l);    
    }


int gcd(int a, int b)
 {
    if (b == 0)
        return a;
    return gcd(b, a % b); 
 }

int sum(struct frac a[],int n){
   int l,t[n],s=0;
    l=lcm(a,n);
      for(int i =0;i<n;i++){
        t[i]=(l/a[i].den)*a[i].num;
        }
   for(int i =0;i<n;i++){
        s+=t[i] ;  
      }
return s;
}
