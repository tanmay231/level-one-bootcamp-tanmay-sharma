//Write a program to add two user input numbers using one function.
#include <stdio.h>

int main()
{
   float n1,n2,sum;
   printf("enter number 1:");
   scanf("%f",&n1);
   printf("\n enter number 2:");
   scanf("%f",&n2);
   sum= n1+n2;
   printf("\nSum of %f and %f is %f",n1,n2,sum);

    return 0;
}