//Write a program to find the sum of n different numbers using 4 functions
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

void input_arr(int n, float a[]);
float sum(int n,float a[]);
void printt(float a);
int main()
{
    int n;
    printf("enter no. of numbers to be added:");
    scanf("%d",&n);
    float a[n];
    input_arr(n,a);
    float s=sum(n,a);
    printt(s);
    
    return 0;
}
void input_arr(int n,float a[]){
 for(int i =0;i<n;i++){
     printf("enter value %d to be added:",i+1);
     scanf("%f",&a[i]);
 }   

}
float sum(int n,float a[]){
    float s=0;
  for(int i =0;i<n;i++){
     s+=a[i];
 }    
 return s;
}
void printt(float a){
printf("sum is %f",a);    
}
